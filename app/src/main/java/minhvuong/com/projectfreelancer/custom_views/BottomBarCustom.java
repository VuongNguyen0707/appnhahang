package minhvuong.com.projectfreelancer.custom_views;

import android.view.View;

import java.util.ArrayList;

/**
 * Created by nguye on 4/12/2018.
 */

public class BottomBarCustom {
    private ArrayList<ItemBottomBar> listItemBottomBar;

    public BottomBarCustom() {
        listItemBottomBar = new ArrayList<>();
    }

    public void setOnClickItemBar(final OnClickMenu onClickMenu) {
        for (final ItemBottomBar itemBottomBar : listItemBottomBar) {
            itemBottomBar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickMenu.onClick(v.getId());
                    for (final ItemBottomBar itemBottomBar : listItemBottomBar) {
                        itemBottomBar.setSelected(false);
                    }
                    itemBottomBar.setSelected(true);
                }
            });
        }
    }

    public void addItem(ItemBottomBar itemBottomBar) {
        listItemBottomBar.add(itemBottomBar);
    }

    public void setActive(int idItemMenu) {
        for (final ItemBottomBar itemBottomBar : listItemBottomBar) {
            itemBottomBar.setSelected(false);
            if (idItemMenu == itemBottomBar.getId()) {
                itemBottomBar.setSelected(true);
            }
        }
    }

    public interface OnClickMenu {
        void onClick(int idItemMenu);
    }
}
