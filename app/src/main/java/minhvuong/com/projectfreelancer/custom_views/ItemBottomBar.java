package minhvuong.com.projectfreelancer.custom_views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import minhvuong.com.projectfreelancer.R;

/**
 * Created by nguye on 4/18/2018.
 */

public class ItemBottomBar extends RelativeLayout {
    private ImageView ivIcon;
    private TextView tvTitle;
    private int resIdActive;
    private int resIdDrawableBackgroundSelected;
    private int resIdDrawableBackgroundNormal;
    private int resIdNormal;
    private int titleColorTextSelected = Color.parseColor("#ffffff");
    private int titleColorTextNormal = Color.parseColor("#D2D2D2");
    private boolean isSelected;

    public ItemBottomBar(Context context) {
        super(context);
        initView();
    }

    public ItemBottomBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);

    }

    public ItemBottomBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context, attrs);
    }

    public ItemBottomBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context, attrs);
    }

    void initView() {
        inflate(getContext(), R.layout.item_bottom_bar, this);
        ivIcon = findViewById(R.id.iv_icon);
        tvTitle = findViewById(R.id.tv_title);
    }

    private void initView(Context context, AttributeSet attrs) {
        /*get attrs*/
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ItemBottomBar);
        String titleText = a.getString(R.styleable.ItemBottomBar_titleText);
        titleColorTextNormal = a.getColor(R.styleable.ItemBottomBar_titleColor, Color.parseColor("#ffffff"));
        titleColorTextSelected = a.getColor(R.styleable.ItemBottomBar_titleColorSelected, Color.parseColor("#ffffff"));
        Drawable icon = a.getDrawable(R.styleable.ItemBottomBar_srcIcon);
        int colorItem = a.getColor(R.styleable.ItemBottomBar_backgroundColor, getContext().getResources().getColor(R.color.colorPrimary));
        resIdDrawableBackgroundSelected = a.getResourceId(R.styleable.ItemBottomBar_backgroundDrawableSelected, -1);
        resIdDrawableBackgroundNormal = a.getResourceId(R.styleable.ItemBottomBar_backgroundDrawableNormal, -1);
        if (resIdDrawableBackgroundNormal == -1) {
            resIdDrawableBackgroundNormal = R.drawable.bg_bottom_bar_normal;
        }
        if (resIdDrawableBackgroundSelected == -1) {
            resIdDrawableBackgroundSelected = resIdDrawableBackgroundNormal;
        }
        setSelected(a.getBoolean(R.styleable.ItemBottomBar_selected, false));
        a.recycle();

        /*initview*/
        inflate(getContext(), R.layout.item_bottom_bar, this);
        ivIcon = findViewById(R.id.iv_icon);
        tvTitle = findViewById(R.id.tv_title);
        if (!TextUtils.isEmpty(titleText)) {
            tvTitle.setText(titleText);
        }
        tvTitle.setTextColor(titleColorTextNormal);
        if (icon != null) {
            ivIcon.setBackground(icon);
        }
//        this.setBackgroundColor(colorItem);

        this.setBackgroundResource(resIdDrawableBackgroundNormal);
    }

    public void setSelected(boolean isSelected) {
//        super.setSelected(isSelected);
        if (isSelected) {
            if (ivIcon != null) {
                ivIcon.setImageResource(resIdActive);
            }

            if (tvTitle != null) {
                tvTitle.setTextColor(titleColorTextSelected);
            }

            this.setBackgroundResource(resIdDrawableBackgroundSelected);
        } else {
            if (ivIcon != null) {
                ivIcon.setImageResource(resIdNormal);
            }

            if (tvTitle != null) {
                tvTitle.setTextColor(titleColorTextNormal);
            }

            this.setBackgroundResource(resIdDrawableBackgroundNormal);
        }
        this.isSelected = isSelected;
    }

}
