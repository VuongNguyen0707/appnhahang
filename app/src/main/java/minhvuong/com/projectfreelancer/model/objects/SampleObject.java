package minhvuong.com.projectfreelancer.model.objects;

/**
 * Created by nguye on 4/12/2018.
 */

public class SampleObject {
    private String tilte;
    private String waitingPayment;
    private String waitingProcess;
    private String payment;
    private String time;
    private String name;

    public SampleObject() {
    }

    public SampleObject(String tilte, String waitingPayment, String waitingProcess, String payment, String time, String name) {
        this.tilte = tilte;
        this.waitingPayment = waitingPayment;
        this.waitingProcess = waitingProcess;
        this.payment = payment;
        this.time = time;
        this.name = name;
    }

    public String getTilte() {
        return tilte;
    }

    public void setTilte(String tilte) {
        this.tilte = tilte;
    }

    public String getWaitingPayment() {
        return waitingPayment;
    }

    public void setWaitingPayment(String waitingPayment) {
        this.waitingPayment = waitingPayment;
    }

    public String getWaitingProcess() {
        return waitingProcess;
    }

    public void setWaitingProcess(String waitingProcess) {
        this.waitingProcess = waitingProcess;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
