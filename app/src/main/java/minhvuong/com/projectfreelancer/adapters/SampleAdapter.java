package minhvuong.com.projectfreelancer.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import minhvuong.com.projectfreelancer.R;
import minhvuong.com.projectfreelancer.model.objects.SampleObject;

/**
 * Created by nguye on 4/12/2018.
 */

public class SampleAdapter extends BaseAdapter {
    private ArrayList<SampleObject> sampleObjects;
    private LayoutInflater inflater;
    private Context context;

    public SampleAdapter(ArrayList<SampleObject> sampleObjects, Context context) {
        this.sampleObjects = sampleObjects;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return sampleObjects.size();
    }

    @Override
    public Object getItem(int position) {
        return sampleObjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_list, parent, false);
            holder = new Holder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        SampleObject sampleObject = sampleObjects.get(position);
        holder.tvTilte.setText(sampleObject.getTilte());
        holder.tvName.setText(sampleObject.getName());
        holder.tvPayment.setText(sampleObject.getPayment());
        holder.tvTime.setText(sampleObject.getTime());
        holder.tvWaitingPayment.setText(sampleObject.getWaitingPayment());
        holder.tvWaitingProcess.setText(sampleObject.getWaitingProcess());
        return convertView;
    }

    @Nullable
    @Override
    public CharSequence[] getAutofillOptions() {
        return new CharSequence[0];
    }

    class Holder {
        TextView tvTilte;
        TextView tvWaitingPayment;
        TextView tvWaitingProcess;
        TextView tvPayment;
        TextView tvTime;
        TextView tvName;

        public Holder(View itemView) {
            tvTilte = itemView.findViewById(R.id.tv_sample);
            tvWaitingPayment = itemView.findViewById(R.id.tv_awating);
            tvWaitingProcess = itemView.findViewById(R.id.tv_awating_process);
            tvPayment = itemView.findViewById(R.id.tv_payment);
            tvTime = itemView.findViewById(R.id.tv_time_detail);
            tvName = itemView.findViewById(R.id.tv_name_sample);
        }
    }
}
