package minhvuong.com.projectfreelancer;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import minhvuong.com.projectfreelancer.custom_views.BottomBarCustom;
import minhvuong.com.projectfreelancer.custom_views.ItemBottomBar;
import minhvuong.com.projectfreelancer.fragment.Fragment1;
import minhvuong.com.projectfreelancer.fragment.Fragment2;
import minhvuong.com.projectfreelancer.fragment.Fragment3;
import minhvuong.com.projectfreelancer.fragment.Fragment4;

public class MainActivity extends AppCompatActivity {
    private BottomBarCustom bottomBarCustom;
    private ItemBottomBar itBottomBar1;
    private ItemBottomBar itBottomBar2;
    private ItemBottomBar itBottomBar3;
    private ItemBottomBar itBottomBar4;
    private int mContainerResId = R.id.content_main;
    private boolean isFinish = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        itBottomBar1 = findViewById(R.id.item_1);
        itBottomBar2 = findViewById(R.id.item_2);
        itBottomBar3 = findViewById(R.id.item_3);
        itBottomBar4 = findViewById(R.id.item_4);
        bottomBarCustom = new BottomBarCustom();

        bottomBarCustom.addItem(itBottomBar1);
        bottomBarCustom.addItem(itBottomBar2);
        bottomBarCustom.addItem(itBottomBar3);
        bottomBarCustom.addItem(itBottomBar4);
        bottomBarCustom.setActive(R.id.item_1);
        switchFragment(Fragment1.newInstance(), true);
        bottomBarCustom.setOnClickItemBar(new BottomBarCustom.OnClickMenu() {
            @Override
            public void onClick(int idMenu) {
                switch (idMenu) {
                    case R.id.item_1:
                        switchFragment(Fragment1.newInstance(), true);
                        break;
                    case R.id.item_2:
                        switchFragment(Fragment2.newInstance(), true);
                        break;
                    case R.id.item_3:
                        switchFragment(Fragment3.newInstance(), true);
                        break;
                    case R.id.item_4:
                        switchFragment(Fragment4.newInstance(), true);
                        break;

                }
            }
        });
    }

    public void switchFragment(final Fragment fragment, final Boolean addToBackStack) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                String NAME = fragment.getClass().getSimpleName();
                FragmentManager fm = getSupportFragmentManager();
                try {
                    boolean fragmentPopped = fm.popBackStackImmediate(NAME, 0);
                    if (fragmentPopped || fm.findFragmentByTag(NAME) != null) {
                        fm.beginTransaction()
                                .replace(mContainerResId, fragment, NAME)
                                .commit();
                    } else {
                        if (!addToBackStack) {
                            fm.beginTransaction()
                                    .replace(mContainerResId, fragment, NAME)
                                    .commit();
                        } else {
                            fm.beginTransaction()
                                    .replace(mContainerResId, fragment, NAME)
                                    .addToBackStack(NAME)
                                    .commit();
                        }
                    }
                } catch (Exception ex) {
                    switchFragment(fragment, addToBackStack);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isFinish) {
            finish();
        } else {
            isFinish = true;
            Toast.makeText(MainActivity.this, "Press again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    isFinish = false;
                }
            }, 2000);
        }
    }
}
