package minhvuong.com.projectfreelancer.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import minhvuong.com.projectfreelancer.R;

/**
 * Created by nguye on 4/12/2018.
 */

public class Fragment2 extends Fragment {

    public static Fragment2 newInstance() {
        Fragment2 instance = new Fragment2();
        instance.setArguments(new Bundle());
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_2, container, false);
    }
}
