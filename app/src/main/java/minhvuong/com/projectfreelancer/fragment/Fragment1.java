package minhvuong.com.projectfreelancer.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import minhvuong.com.projectfreelancer.R;
import minhvuong.com.projectfreelancer.model.objects.SampleObject;
import minhvuong.com.projectfreelancer.adapters.SampleAdapter;

/**
 * Created by nguye on 4/12/2018.
 */

public class Fragment1 extends Fragment {
    private SampleAdapter sampleAdapter;
    private View rootView;
    private ListView lvSample;
    private ArrayList<SampleObject> sampleObjects;
    private ArrayList<SampleObject> sampleObjects2;
    private RelativeLayout tab1;
    private RelativeLayout tab2;
    private TextView tvTab2;
    private TextView tvTab1;

    public static Fragment1 newInstance() {
        Fragment1 instance = new Fragment1();
        instance.setArguments(new Bundle());
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_1, container, false);
        initView();
        initData();
        return rootView;
    }

    private void initData() {
        sampleObjects = new ArrayList<>();
        sampleObjects2 = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            sampleObjects.add(new SampleObject("Sample - " + i, "AWAITING PAYMENT" + i, "AWAITING PROCESSING" + i, "$24.26", "3 items, sep 25, 2009", "Wendy Darling [sample]"));
            sampleObjects2.add(new SampleObject("Sample List 2 - " + i, "AWAITING PAYMENT" + i, "AWAITING PROCESSING" + i, "$24.26", "3 items, sep 25, 2009", "Wendy Darling [sample]"));
        }
        sampleAdapter = new SampleAdapter(sampleObjects, getContext());
        lvSample.setAdapter(sampleAdapter);
    }

    private void initView() {
        lvSample = rootView.findViewById(R.id.lv_sample);
        tab1 = rootView.findViewById(R.id.tab_1);
        tab2 = rootView.findViewById(R.id.tab_2);
        tvTab1 = rootView.findViewById(R.id.tv_tab_1);
        tvTab2 = rootView.findViewById(R.id.tv_tab_2);
        tab1.setBackgroundResource(R.drawable.bg_tab_selected);
        tab2.setBackgroundResource(R.drawable.bg_tab_normal);
        tab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tab1.setBackgroundResource(R.drawable.bg_tab_selected);
                tab2.setBackgroundResource(R.drawable.bg_tab_normal);
                tvTab1.setTextColor(getContext().getResources().getColor(R.color.bottom_bar_selected));
                tvTab2.setTextColor(getContext().getResources().getColor(R.color.bottom_bar_normal));
                sampleAdapter = new SampleAdapter(sampleObjects, getContext());
                lvSample.setAdapter(sampleAdapter);
            }
        });

        tab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tab1.setBackgroundResource(R.drawable.bg_tab_normal);
                tab2.setBackgroundResource(R.drawable.bg_tab_selected);
                sampleAdapter = new SampleAdapter(sampleObjects2, getContext());
                lvSample.setAdapter(sampleAdapter);
                tvTab1.setTextColor(getContext().getResources().getColor(R.color.bottom_bar_normal));
                tvTab2.setTextColor(getContext().getResources().getColor(R.color.bottom_bar_selected));
            }
        });
    }
}
